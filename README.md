### Stacks
go
gin
grpc
kubernetes
bitbucket pipelines

### Generate .pb.go file
> cd backend-service/pkg
> protoc -I greeter greeter/service.proto --go_out=plugins=grpc:greeter

[Note] Assume that api-gateway and backend-service are in the separate private repositories.

## Setting up bitbucket pipelines
### Pipeline Environment Variables
1. In Bitbucket, goto Settings
2. In Pipelines section, goto Environment Variables
3. Add variables
> GCLOUD_CLUSTER
> GCLOUD_PROJECT_DEV
> GCLOUD_ZONE
> PROJECT_NAME
> GCLOUD_API_KEYFILE_DEV*
4. How to generate GCLOUD_API_KEYFILE_DEV*
4.1 Download Service Account json file
4.2 Convert to base64
> base64 <your-credentials-file.json>

### Bitbucket SSH key
As api-gateway need to get dependency file from backend-service when building go binary, I chose to build the go binary in the pipeline instead of in Dockerfile as in backend-service.

1. In Bitbucket, under backend-service, Pipelines section, goto  SSH keys
2. Generate Keys (In this step, secret key is already available in the pipelines)
3. Add public key to api-service in GENERAL > Access Keys
