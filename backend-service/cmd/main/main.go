package main

import (
	"context"
	"fmt"
	"log"
	"net"

	pb "bitbucket.org/tanopwan/go-gin-grpc-gcd/backend-service/pkg/greeter"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const (
	host = ""
	port = "5300"
)

// Server ... will contain grpc interfaces
type Server struct{}

// Hello ... will accept email and create firebase account and return uid
func (s *Server) Hello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	message := "Hello, " + in.Name
	return &pb.HelloReply{Message: message}, nil
}

func main() {
	fmt.Println("Starting backend-service ...")
	lis, err := net.Listen("tcp", host+":"+port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &Server{})
	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
