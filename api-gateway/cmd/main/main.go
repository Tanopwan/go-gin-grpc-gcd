package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	pb "bitbucket.org/tanopwan/go-gin-grpc-gcd/backend-service/pkg/greeter"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
)

const (
	greeterHost = ""
	greeterPort = "5300"
	serverPort  = "3000"
)

func main() {

	conn, err := grpc.Dial(greeterHost+":"+greeterPort, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Dial failed: %v", err)
	}
	defer conn.Close()
	client := pb.NewGreeterClient(conn)

	router := gin.Default()

	router.GET("/hello", func(c *gin.Context) {
		fmt.Printf("name: %s\n", c.Query("name"))
		res, err := client.Hello(context.Background(), &pb.HelloRequest{Name: c.Query("name")})
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"message-from-backend": res.Message,
		})
	})

	router.Run(":" + serverPort)
}
